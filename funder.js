const axios = require('axios');
const fundlist = require('./fundlist');
const Alarm = require('./alarm');

const FUND_API = "https://apis.fundrich.com.tw/default/v1/funds/navPrices/";

function add(a, b) {
  return ((a*1000 + b*1000)/1000);
}

function divide(a, b) {
  return Number(((a*1000) / (b*1000)).toFixed(3));
}

function between(min, max) {  
  return Math.floor(
    Math.random() * (max - min) + min
  );
}

function calNav(fundId, navs, len) {
  let ma30 = 0;
  let ma72 = 0;

  for(let i=navs.length-1,j=0;j<72 && i >= 0;i--,j++) {
    if(j < 30) {
      ma30 = add(ma30, navs[i].Price);
    }

    if(j < 72) {
      ma72 = add(ma72, navs[i].Price);
    }
  }

  ma30 = divide(ma30, 30);
  ma72 = divide(ma72, 72);
  
  let ma3072 = divide(ma30 + ma72, 2); 
  let div = divide((navs[navs.length-1].Price - ma3072)*100, ma3072);

  return div;
}

function _callback(fundId, list, len) {
  if(len > 72) {
    const div = calNav(fundId, list, len);

    console.log(`${list[len-1].TransDate} ${fundId}, NAV ${list[len-1].Price}, ${div}`);

    fundlist.update(fundId, list[len-1].Price, div, list[len-1].TransDate);
  }
}

function getFundNav(id, duration, callback) {
  axios.get(FUND_API + id + "?duration=" + duration)
  .then(res => {
    if(callback) {
      callback(id, res.data, res.data.length);
    }
  })
  .catch(err => {
    // console.log(err);
    //console.log("err: " + err.config.url + ", id " + id);

    setTimeout(getFundNav, 5000, id, "4m", _callback);
  })
}

function getFundsNavDiv() {
  fundlist.all(0, (funds) => {
    console.log("start to update " + funds.length + " funds");
    let timeout = 0;
  
    for(let i=0;i<funds.length;i++) {
      if(i != 0 && ((i % 10) == 0)) {
        timeout += (between(3,5) * 1000);
      }
      else {
        timeout += (between(1,5) * 100);
      }
      
      setTimeout(getFundNav, timeout, funds[i].fundId, "4m", _callback);
    }
  });
}

module.exports = {
  get: (id, duration, callback) => {
    axios.get(FUND_API + id + "?duration=" + duration)
    .then(res => {
      if(callback) {
        callback(id, res.data, res.data.length);
      }
    })
    .catch(err => {
      // catch
      // console.log("err: " + err.config.url + ", id " + id);
    })
  },
};

// every 6:30am
const poll = new Alarm(getFundsNavDiv, 6, 30);

// every 2pm
const poll2 = new Alarm(getFundsNavDiv, 15, 0);
