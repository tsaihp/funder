FROM node:16.13 as build

WORKDIR /funder

COPY . .

RUN npm install

FROM node:16.13-alpine

LABEL maintainer="tsaihp@gmail.com"

RUN apk update \
    && apk add tzdata \
    && cp /usr/share/zoneinfo/Asia/Taipei /etc/localtime \
    && echo "Asia/Taipei" > /etc/timezone \
    && apk del tzdata \
    && rm -rf /var/cache/apk/*

COPY --from=build /funder /

EXPOSE 3000
CMD [ "npm", "run", "start" ]
