class Alarm {
  constructor(callback, hr, min) {
    this.callback = callback;
    this.hr = hr;
    this.min =min;

    this.start();
  }
  start() {
    const current = new Date();
    this.alarm = new Date();
    if(current.getHours() > this.hr ||
        (current.getHours() == this.hr && current.getMinutes() >= this.min)) {
      this.alarm.setDate(this.alarm.getDate()+1);
    }

    this.alarm.setHours(this.hr);
    this.alarm.setMinutes(this.min);
    this.alarm.setSeconds(0);
    this.alarm.setMilliseconds(0);

    console.log(`next start at ${this.alarm.toLocaleString()}`);

    this.alarm = setTimeout(() => {
      this.callback();
      this.start();
    }, (this.alarm-current));
  }
  stop() {
    clearTimeout(this.alarm);
  }
}

module.exports = Alarm;