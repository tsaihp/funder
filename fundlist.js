const axios = require('axios');
const sqlite3 = require('sqlite3').verbose();
const FUND_LIST_API = "https://apis.fundrich.com.tw/FrsWebApi/Common/ThemeFund/FundsDataInfo";

function between(min, max) {  
  return Math.floor(
    Math.random() * (max - min) + min
  )
}

// test
function getFundList(pageNo) {
  axios.post(FUND_LIST_API, {page: pageNo})
  .then(res => {
    for(let i=0;i<res.data.items.length;i++) {
      console.log(`${i} ${res.data.items[i].id} ${res.data.items[i].name.text} ${res.data.items[i].extent.value} ${res.data.items[i].extent.currency} ${res.data.items[i].isSell}`)
      // getFundNav(res.data.items[i].id, "4m", calNav);

      const fund = res.data.items[i];

      db_insert(fund.id, fund.name.text, fund.extent.currency, fund.extent.size, fund.isSell);
      // db_update(fund.id, fund.extent.value);
    }

    const pageData = res.data.BkEzData;

    if(pageData.condition.currentPage * pageData.condition.countPerPage < pageData.count) {
      console.log(`page ${pageData.condition.currentPage+1}`);
      setTimeout(getFundList, (between(2, 5)*1000), pageData.condition.currentPage+1);
    }
  })
  .catch(err => {
    // error
    console.log('err to get');
    console.log(err);
    setTimeout(getFundList, (10*1000), JSON.parse(err.config.data).page);
  })
}

// var fs = require("fs");
var file = "./test.db";

let db = new sqlite3.Database(file);

db_create();

// getFundList(1);

// db_show();

function db_close() {
  db.close();
}

function db_show() {
  db.serialize(() => {
    db.all("SELECT * from funds", (err, res) => {
      if(!err) {
        console.log('funds: ' + res.length);
        // for(let i=0;i<res.length;i++) {
        //   console.log(`${res[i].fundId} ${res[i].name}`);
        // }
      }
      else {
        console.log("err");
      }
    });

    db.all("SELECT * FROM fundnavs", (err, res) => {
      if(!err) {
        console.log('navs: ' + res.length);
        // for(let i=0;i<res.length;i++) {
        //   console.log(`${res[i].fundId}`);
        // }
      }
      else {
        console.log("err");
      }
    });
  });
}

function db_create() {
  console.log('db_create()');

  db.serialize(() => {
    // funds
    db.run("CREATE TABLE IF NOT EXISTS funds \
            (\
              name TEXT,\
              fundId TEXT, \
              currency TEXT, \
              isSell BOOLEAN, \
              size DOUBLE \
            )"
          );

    // fundnavs
    db.run("CREATE TABLE IF NOT EXISTS fundnavs \
            (\
              fundId TEXT, \
              lastUpdate DATE,\
              nav FLOAT,\
              div FLOAT\
            )"
          );
    });
}

function db_insert(fundId, name, currency, size, isSell) {
    const sqlInsert = "INSERT OR IGNORE INTO funds(name,fundId,currency,size,isSell) VALUES (?,?,?,?,?) ";
    const sqlNavInsert = "INSERT OR IGNORE INTO fundnavs(fundId) VALUES (?) ";

    // console.log("db_insert()");

    db.run(sqlInsert, [name, fundId, currency, size, isSell]);
    db.run(sqlNavInsert, [fundId]);
}

function db_update(fundId, nav, div, updated) {
  let db = new sqlite3.Database(file);

  db.serialize(() => {
    db.run("CREATE TABLE IF NOT EXISTS funds \
            (\
              name TEXT,\
              fundId TEXT, \
              currency TEXT, \
              lastUpdate DATE,\
              nav FLOAT,\
              div FLOAT)");

    const sqlUpdate = " \
      UPDATE funds \
      SET nav=?, div=? \
      WHERE fundId=? \
    ";

    db.run(sqlUpdate, [nav, 0.1, fundId], (err, res) => {
      if(err) {
        console.log(err);
      }
      else {
        // console.log(res);
      }
    });

    db.close();
  });
}

db_show();

module.exports = {
  get: (offset, count, filter) => {
    return new Promise( async (resolve, reject) => {
      offset = (offset) ? offset : 0;
      count = (count) ? count : 10;

      const date = new Date();
      date.setDate(date.getDate() - 7);

      const monthString = `${((date.getMonth()+1) < 10) ? '0' : ''}${date.getMonth()+1}`;
      const dayString = `${(date.getDate() < 10) ? '0' : ''}${date.getDate()}`;
      const dateString = `${date.getFullYear()}${monthString}${dayString}`;

      let sqlFilter = "";

      if(filter) {
        filter = JSON.parse(filter); // TODO

        if(filter.currency.length) {
          for(let i=0;i<filter.currency.length;i++) {
            if(i > 0) {
              sqlFilter += 'OR';  
            }
            sqlFilter += ` currency = '${filter.currency[i]}' `;
          }

          sqlFilter = "AND" + sqlFilter;
        }
      }

      let query = 
        `SELECT funds.fundId, name, currency, nav, div, lastUpdate 
            FROM fundnavs JOIN funds ON fundnavs.fundId = funds.fundId 
            WHERE div IS NOT NULL AND isSell IS 1 
              AND lastUpdate IS NOT NULL AND lastUpdate >= ${dateString} ${sqlFilter}
            ORDER BY div ASC 
            LIMIT ${count} OFFSET ${offset};`;

      db.all(query, (err, rows) => {
        if(err) {
          return reject(err);
        }
        resolve({list: rows});
      });
    });
  },
  count: (filter) => {
    return new Promise( async (resolve, reject) => {
      let sqlFilter = "";

      if(filter) {
        filter = JSON.parse(filter); // TODO

        if(filter.currency.length) {
          for(let i=0;i<filter.currency.length;i++) {
            if(i > 0) {
              sqlFilter += 'OR';  
            }
            sqlFilter += ` currency = '${filter.currency[i]}' `;
          }

          sqlFilter = "AND" + sqlFilter;
        }
      }

      let query = 
        `SELECT funds.fundId, name, currency, nav, div, lastUpdate 
            FROM fundnavs JOIN funds ON fundnavs.fundId = funds.fundId 
            WHERE div IS NOT NULL AND isSell IS 1 ${sqlFilter};`;

      db.all(query, (err, rows) => {
        if(err) {
          return reject(err);
        }
        resolve(rows.length);
      });
    });
  },
  all: (delay, callback) => {
    db.serialize(() => {
      db.all("SELECT * FROM funds WHERE currency = 'TWD' or currency = 'USD'", (err, rows) => {
        callback(rows);
      });
    });
  },
  update: (fundId, nav, div, updated) => {
    const sqlUpdate = " \
      UPDATE fundnavs \
      SET nav=?, div=?, lastUpdate=? \
      WHERE fundId=? \
    ";

    db.run(sqlUpdate, [nav, div, updated, fundId], (err, res) => {
      if(err) {
        console.log(err);
      }
    });
  }
};

// db.all("SELECT * FROM funds WHERE currency = 'TWD'", (err, rows) => {
//   console.log(rows.length);
// });

// db.serialize(() => {
//   db.each("SELECT fundId FROM funds WHERE currency = 'TWD'", (err, row) => {
//     // console.log(row.name);
//     const sqlUpdate = " \
//       UPDATE fundnavs \
//       SET nav=? \
//       WHERE fundId=? \
//     ";

//     db.run(sqlUpdate, [0.1, row.fundId], (err, res) => {
//       if(err) {
//         console.log(err);
//       }
//     });
//   });

//   db.each("SELECT fundId, nav FROM fundnavs WHERE currency = 'TWD'", (err, row) => {

//   });
// });
