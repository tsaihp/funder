const express = require('express');
const funder = require('./funder');
const fundlist = require('./fundlist');
const app = express();
const LISTEN_PORT = 3000;

// const stockq = require('./cnyes');

// static
app.use('/funds', express.static('static'));

app.get('/funds/apis/funds', async (req, res) => {
  const offset = (req.query.offset) ? req.query.offset : 0;
  const count = (req.query.count) ? req.query.count : 10;
  let funds = await fundlist.get(Number(offset), Number(count), req.query.filter);
  let total = await fundlist.count(req.query.filter);

  res.json({
    status: "OK",
    funds: funds.list,
    num: total,
  });
});

app.get('/funds/apis/:id', (req, res) => {
  res.json({
    status: "OK",
  });
});

app.listen(LISTEN_PORT, () => {
  console.log(`App listening on port ${LISTEN_PORT}!`)
});